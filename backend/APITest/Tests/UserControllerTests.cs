﻿using Core.DTOs;
using Core.Managers;
using Core;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WebApp.Controllers;

namespace WebAppTest.Tests
{
    public class UserControllerTests
    {
        private readonly UserController _userController;
        private readonly Mock<IUserManager> userManagerMock;

        public UserControllerTests()
        {
            // Configurar controlador con un mock del manager
             userManagerMock = new Mock<IUserManager>();
            _userController = new UserController(userManagerMock.Object); //paso el Object para que sea mas directo; ya que Objec devuelve una instancia del objeto simulado.
        }

        [Fact]
        public async Task GetAllUsers_ReturnsOkResult()
        {
            // Arrange
            var expectedUsers = new List<UserDTO> { new UserDTO { Id = 1, Name = "User1", Age = 25 } };
            var resultOperation = new ResultOperation<List<UserDTO>>
            {
                Data = expectedUsers,
                IsSuccess = true,
                Message = "Operación exitosa"
            };
            userManagerMock.Setup(manager => manager.GetAllUsersAsync()).ReturnsAsync(resultOperation);//Definimos el comportamiento del mock con Setup

            // Act
            var result = await _userController.GetAllUser();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

    }
}
