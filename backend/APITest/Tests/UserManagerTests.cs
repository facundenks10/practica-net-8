﻿using AutoMapper;
using Core;
using Core.DTOs;
using Core.Managers;
using Data.Entities;
using Data.Repositories;
using Moq;

namespace WebAppTest.Tests
{
    public class UserManagerTests
    {
        private readonly UserManager _userManager;
        private readonly Mock<IUserRepository> _userRepositoryMock;

        public UserManagerTests()
        {
            // Configurar un mock del repositorio de usuarios
            _userRepositoryMock = new Mock<IUserRepository>();

            // Configurar el mapeo
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapping>();
            });

            var mapper = mapperConfiguration.CreateMapper();

            _userManager = new UserManager(mapper, _userRepositoryMock.Object, new Mock<IProductoManager>().Object);
        }

        [Fact]
        public async Task GetAllUsersAsync_ReturnsListOfUsers()
        {
            // Arrange
            var expectedUsers = new List<UserEntity>();
            expectedUsers.Add(new UserEntity { Name = "User1", Password = "12345", UserName = "Test1", Email = "user1@hotmail.com", Age = 25 });
            expectedUsers.Add(new UserEntity { Name = "User2", Password = "12345", UserName = "Test2", Email = "user1@hotmail.com", Age = 30 });

            //await _userManager.CreateUserAsync(new UserDTOForPost { Name = "User1", Age = 25 });
            _userRepositoryMock.Setup(repo => repo.GetAll()).ReturnsAsync(expectedUsers);

            // Act
            var result = await _userManager.GetAllUsersAsync();

            // Assert
            Assert.True(result.IsSuccess);
            Assert.Equal(expectedUsers.Count, result.Data.Count);
            Assert.True(expectedUsers.Count > 0);
        }
    }
}
