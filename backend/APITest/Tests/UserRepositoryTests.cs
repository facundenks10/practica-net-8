﻿using Data.Entities;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Data.Db;

namespace WebAppTest.Tests
{
    public class UserRepositoryTests
    {
        private readonly DexDbContext _dbContext;

        public UserRepositoryTests()
        {
            //JPG: Configurar contexto de base de datos en memoria
            var options = new DbContextOptionsBuilder<DexDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;
            _dbContext = new DexDbContext(options);
        }

        [Fact]
        public async Task GetAll_ReturnsListOfEntities()
        {
            // Arrange
            var expectedUsers = new List<UserEntity> { };
            await SeedDatabaseWithUsers(expectedUsers);

            // Act
            var repository = new UserRepository(_dbContext);
            var result = await repository.GetAll();

            // Assert
            Assert.Equal(expectedUsers.Count, result.Count);
        }

        [Fact]
        public async Task UpdateUser_IsCorrect()
        {
            // Arrange
            var userIdToTest = 1;
            var expectedUsers = new List<UserEntity> { new UserEntity { Id = userIdToTest, Password="12345", UserName="Test1", Email = "userTest@hotmail.com", Name = "User1", Age = 25 } };
            await SeedDatabaseWithUsers(expectedUsers);

            // Act
            var repository = new UserRepository(_dbContext);
            var savedUser = await repository.GetByID(userIdToTest);
            if (savedUser == null) throw new Exception("El usuario recuperado es nulo.");
            savedUser.Name = "User1_upd";
            savedUser.Age = 1;
            await repository.Update(savedUser);

            // Assert
            var currenUser = await repository.GetByID(userIdToTest);
            Assert.NotNull(currenUser);
            Assert.Equal("User1_upd", currenUser.Name);
        }

        private async Task SeedDatabaseWithUsers(List<UserEntity> users)
        {
            foreach (var user in users)
            {
                _dbContext.Users.Add(user);
            }
            await _dbContext.SaveChangesAsync();
        }
    }
}
