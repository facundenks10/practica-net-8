﻿using AutoMapper;
using Core.DTOs;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<ProductoEntity, ProductoDTO>().ReverseMap();
            CreateMap<ProductoEntity, ProductoDTOForPost>().ReverseMap();

            CreateMap<UserEntity, UserDTO>().ReverseMap();
            CreateMap<UserEntity, UserDTOForPost>().ReverseMap();
            CreateMap<UserEntity, UserWithProductosDTO>().ReverseMap();
            CreateMap<UserEntity, UserForUpdateDTO>().ReverseMap();

        }
    }
}
