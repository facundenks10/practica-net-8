﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTOs
{
    public class ProductoDTO
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int? Stock { get; set; }
        public decimal Precio { get; set; }
        public int UserId { get; set; }
    }
}
