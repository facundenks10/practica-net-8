﻿
namespace Core.DTOs
{
    public class ProductoDTOForPost
    {
        public string Descripcion { get; set; }
        public int? Stock { get; set; }
        public decimal Precio { get; set; }
        public int UserId { get; set; }
    }
}
