﻿namespace Core.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Code { get; set; }

        //public ICollection<ProductoDTO> Productos { get; set; }
    }
}
