﻿using Core.DTOs;

namespace Core.Managers
{
    public interface IProductoManager
    {
        // CREATE
        Task<ResultOperation<ProductoDTO>> CreateProductAsync(ProductoDTOForPost newProduct);

        // READ
        Task<ResultOperation<ProductoDTO>> GetProductByIdAsync(int id);

        //LIST
        Task<ResultOperation<List<ProductoDTO>>> GetAllProductsAsync();

        // UPDATE
        Task<ResultOperation<ProductoDTO>> UpdateProductAsync(ProductoDTO product);

        // DELETE
        Task<ResultOperation<bool>> DeleteProductAsync(int id);

        Task<List<ProductoDTO>> GetProductsByUser(int userId);
    }
}
