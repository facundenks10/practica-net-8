﻿using Core.DTOs;
using Data.Entities;

namespace Core.Managers
{
    public interface IUserManager
    {
        // CREATE
        Task<ResultOperation<UserDTO>> CreateUserAsync(UserDTOForPost newUser);

        // READ
        Task<ResultOperation<UserDTO>> GetUserByIdAsync(int id);
        Task<ResultOperation<UserDTO>> GetUserByEmailAsync(string email);
        //LIST
        Task<ResultOperation<List<UserDTO>>> GetAllUsersAsync();

        // UPDATE
        Task<ResultOperation<UserDTO>> UpdateUserAsync(UserForUpdateDTO user);
        
        //UPDATE CODE
        Task<ResultOperation<UserDTO>> UpdateUserCodeAsync(string email, string code);

        // DELETE
        Task<ResultOperation<bool>> DeleteUserAsync(int id);

        //GET ALL INFO
        Task<ResultOperation<List<UserWithProductosDTO>>> GetUserWithProductos(int page);
    }
}
