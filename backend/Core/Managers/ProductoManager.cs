﻿using AutoMapper;
using Core.DTOs;
using Data.Db;
using Data.Entities;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace Core.Managers
{
    public class ProductoManager : IProductoManager
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;


        public ProductoManager(IMapper mapper, IProductRepository productRepository)
        {
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public async Task<ResultOperation<List<ProductoDTO>>> GetAllProductsAsync()
        {
            var result = new ResultOperation<List<ProductoDTO>>();
            try
            {
                var products = await _productRepository.GetAll() ?? [];
                result.Data = _mapper.Map<List<ProductoDTO>>(products);
                result.IsSuccess = true;
            }
            catch (DbException e)
            {
                result.IsSuccess = false;
                result.Message = e.Message;
            }
            return result;
        }


        public async Task<ResultOperation<ProductoDTO>> GetProductByIdAsync(int id)
        {
            var result = new ResultOperation<ProductoDTO>();
            try
            {
                var product = await _productRepository.GetByID(id);
                if (product == null)
                {
                    result.Message = "Producto NO encontrado";
                    return result;
                };
                result.Data = _mapper.Map<ProductoDTO>(product);
                result.IsSuccess = true;
            }
            catch (DbException e)
            {
                result.Message += e.Message;
            }
            return result;
        }

        public async Task<ResultOperation<ProductoDTO>> CreateProductAsync(ProductoDTOForPost newProduct)
        {
            var result = new ResultOperation<ProductoDTO>();
            try
            {
                var producto = _mapper.Map<ProductoEntity>(newProduct);
                await _productRepository.Insert(producto);
                result.IsSuccess = true;
                result.Message = "Producto creado exitosamente";
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<ResultOperation<ProductoDTO>> UpdateProductAsync(ProductoDTO productDto)
        {
            var result = new ResultOperation<ProductoDTO>();
            try
            {

                var productFound = await _productRepository.GetByID(productDto.Id);

                if (productFound == null)
                {
                    result.IsSuccess = true;
                    result.Message = "El producto no exite";
                    return result;
                }

                var userIdFound = await _productRepository.GetProductsByUser(productDto.UserId);

                if (userIdFound.Count == 0)
                {
                    result.IsSuccess = true;
                    result.Message = "El userId no valido";
                    return result;
                }

                await _productRepository.Update(productFound);
                result.IsSuccess = true;
                result.Message = "Producto actualizado correctamente";

            }
            catch (Exception e)
            {
                result.IsSuccess = false;
                result.Message = "Ocurrio un error al actualizar el producto:" + e.Message;
            }
            return result;
        }

        public async Task<ResultOperation<bool>> DeleteProductAsync(int id)
        {
            var result = new ResultOperation<bool>();
            try
            {
                var product = await _productRepository.GetByID(id);
                if (product == null)
                {
                    result.Message = "NO se encontro el producto";
                    return result;
                }

                await _productRepository.Delete(product);
                result.IsSuccess = true;
                result.Message = "Producto eliminado correctamente";
            }
            catch (DbException e)
            {
                result.IsSuccess = false;
                result.Message = e.Message;
            }
            return result;
        }

        public async Task<List<ProductoDTO>> GetProductsByUser(int userId)
        {
            var products = _mapper.Map<List<ProductoDTO>>(
                await _productRepository.GetProductsByUser(userId));
            return products;
        }
    }
}
