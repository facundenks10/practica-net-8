﻿using AutoMapper;
using Core.DTOs;
using Data.Db;
using Data.Entities;
using Data.Repositories;

namespace Core.Managers
{
    public class UserManager : IUserManager
    {
        private readonly IMapper _mapper;
        private readonly IProductoManager _productManager;
        private readonly IUserRepository _userRepository;

        public UserManager(IMapper mapper, IUserRepository userRepository, IProductoManager productManager)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _productManager = productManager;
        }

        public async Task<ResultOperation<List<UserDTO>>> GetAllUsersAsync()
        {
            var result = new ResultOperation<List<UserDTO>>();

            try
            {
                var users = await _userRepository.GetAll() ?? [];
                result.Data = _mapper.Map<List<UserDTO>>(users);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error al recuperar users: " + ex.Message;
            }

            return result;
        }


        public async Task<ResultOperation<UserDTO>> GetUserByIdAsync(int id)
        {
            var result = new ResultOperation<UserDTO>();
            try
            {
                var user = await _userRepository.GetByID(id);
                if (user == null)
                {
                    result.Message = "User NO encontrado";
                    return result;
                };
                result.Data = _mapper.Map<UserDTO>(user);
                result.IsSuccess = true;
            }
            catch (Exception e)
            {
                result.Message += e.Message;
            }

            return result;
        }

        public async Task<ResultOperation<UserDTO>> GetUserByEmailAsync(string emial)
        {
            var result = new ResultOperation<UserDTO>();
            try
            {
                var user = await _userRepository.GetUserByEmail(emial);
                if (user == null)
                {
                    result.Message = "User NO encontrado";
                    return result;
                };
                result.Data = _mapper.Map<UserDTO>(user);
                result.IsSuccess = true;
            }
            catch (Exception e)
            {
                result.Message += e.Message;
            }

            return result;
        }

        public async Task<ResultOperation<UserDTO>> CreateUserAsync(UserDTOForPost newUser)
        {
            var result = new ResultOperation<UserDTO>();
            try
            {
                var user = _mapper.Map<UserEntity>(newUser);
                await _userRepository.Insert(user);
                result.IsSuccess = true;
                result.Message = "User creado exitosamente";
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public async Task<ResultOperation<UserDTO>> UpdateUserAsync(UserForUpdateDTO userDto)
        {
            var result = new ResultOperation<UserDTO>();
            try
            {
                var userExist = await _userRepository.GetByID(userDto.Id);

                if (userExist == null)
                {
                    result.Message = "Id no encontrado";
                    result.IsSuccess = true;
                    return result;
                }
                userExist.UserName = userDto.UserName;
                userExist.Password = userDto.Password;
                userExist.Name = userDto.Name;
                userExist.Age = userDto.Age;
                userExist.Email = userDto.Email;
                var user = _mapper.Map<UserEntity>(userExist);
                await _userRepository.Update(user);
                result.IsSuccess = true;
                result.Message = "User actualizado correctamente";
            }
            catch (Exception e)
            {
                result.IsSuccess = false;
                result.Message = "Error al actualizar al user: " + e.Message;
            }

            return result;
        }

        public async Task<ResultOperation<UserDTO>> UpdateUserCodeAsync(string email, string code)
        {
            var result = new ResultOperation<UserDTO>();
            try
            {
                var userExist = await _userRepository.GetUserByEmail(email);

                if (userExist == null)
                {
                    result.Message = "Email no encontrado";
                    result.IsSuccess = true;
                    return result;
                }
                userExist.Code = code;
                var user = _mapper.Map<UserEntity>(userExist);
                await _userRepository.Update(user);
                result.IsSuccess = true;
                result.Message = "User Code actualizado correctamente";
            }
            catch (Exception e)
            {
                result.IsSuccess = false;
                result.Message = "Error al actualizar al user: " + e.Message;
            }

            return result;
        }

        public async Task<ResultOperation<bool>> DeleteUserAsync(int id)
        {
            var result = new ResultOperation<bool>();
            try
            {
                var user = await _userRepository.GetByID(id);
                if (user == null)
                {
                    result.Message = "NO se encontro al user";
                    return result;
                }

                var products = await _productManager.GetProductsByUser(user.Id);
                if (products.Count > 0)
                {
                    result.Message = "El user posee productos relacionados";
                    return result;
                }

                await _userRepository.Delete(user);
                result.IsSuccess = true;
                result.Message = "User eliminado correctamente";
            }
            catch (Exception e)
            {

                result.IsSuccess = false;
                result.Message = e.Message;
            }

            return result;
        }

        public async Task<ResultOperation<List<UserWithProductosDTO>>> GetUserWithProductos(int page)
        {
            var result = new ResultOperation<List<UserWithProductosDTO>>();
            try
            {
                if (page == 0) page = 1;
                const int pageSize = 10;
                var skipAmount = (page - 1) * pageSize;

                var users = await GetUsersWithBasicInfo(skipAmount, pageSize);

                result.IsSuccess = true;
                result.Data = users;
                return result;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                return result;
            }
        }

        private async Task<List<UserWithProductosDTO>> GetUsersWithBasicInfo(int skipAmount, int pageSize)
        {
            var users = await _userRepository.GetUserAndProducts(skipAmount, pageSize);

            return _mapper.Map<List<UserWithProductosDTO>>(users);
        }

    }
}
