﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ResultOperation<T>
    {
        public T Data { get; set; } //El objeto que se devuelve
        public bool IsSuccess { get; set; } = false;
        public string Message { get; set; }

        public ResultOperation() { }
    }
}
