﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Db
{
    public class DexDbContext : DbContext
    {
        public DexDbContext(DbContextOptions<DexDbContext> options) : base(options)
        {
        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ProductoEntity> Productos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //JPG: Para pasar a singular
            //modelBuilder.Entity<ProductoEntity>().ToTable("Producto");
            //modelBuilder.Entity<UserEntity>().ToTable("User");
            //para definir decimales
            modelBuilder.Entity<ProductoEntity>()
            .Property(p => p.Precio)
            .HasColumnType("decimal(18,2)");
            //para establecer FK
            modelBuilder.Entity<ProductoEntity>()
            .HasOne(o => o.User)
            .WithMany(u => u.Productos)
            .HasForeignKey(o => o.UserId);
            //para restringir eliminacion, se podria agregara la anterior
            modelBuilder.Entity<ProductoEntity>()
           .HasOne(p => p.User)
           .WithMany(u => u.Productos)
           .HasForeignKey(p => p.UserId)
           .OnDelete(DeleteBehavior.Restrict);
        }

    }
}
