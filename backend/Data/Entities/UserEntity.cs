﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    [Table("User")]
    public class UserEntity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public required string UserName { get; set; }

        [Required]
        public required string Password { get; set; }

        [Required]
        public string? Name { get; set; }

        [Required]
        public required string Email { get; set; }

        public int Age { get; set; }

        public string? Code {  get; set; }
        public virtual ICollection<ProductoEntity>? Productos { get; set; }
    }
}
