﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IProductRepository: IRepositoryAsync<ProductoEntity>
    {
        public Task<List<ProductoEntity>> GetProductsByUser(int userId);
    }
}
