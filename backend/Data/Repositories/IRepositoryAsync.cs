﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IRepositoryAsync<T> : IDisposable where T : class
    {
        Task<List<T>> GetAll();
        Task<T?> GetByID(int id);
        //Task<T?> GetByEmail(string email);
        Task<T> Insert(T entity);
        Task Delete(T entity);
        Task Update(T entity);
    }
}
