﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IUserRepository : IRepositoryAsync<UserEntity>
    {
        public Task<List<UserEntity>> GetUserAndProducts(int skipAmount, int pageSize);
        Task<UserEntity> GetUserByEmail(string email);
    }
}
