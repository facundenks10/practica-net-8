﻿using Data.Db;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Data.Repositories
{
    public class ProductRepository: RepositoryAsync<ProductoEntity>, IProductRepository
    {
        private readonly DexDbContext _context;


        public ProductRepository(DexDbContext context): base (context)
        {
            _context = context;
        }

        public async Task<List<ProductoEntity>> GetProductsByUser(int userId)
        {
            return await _context.Productos.Where(producto => producto.UserId == userId).ToListAsync();
        }
    }
}
