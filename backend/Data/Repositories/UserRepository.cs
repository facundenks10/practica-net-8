﻿using Data.Db;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Data.Repositories
{
    public class UserRepository: RepositoryAsync<UserEntity>, IUserRepository
    {
        private readonly DexDbContext _context;

        public UserRepository(DexDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<UserEntity>> GetUserAndProducts(int skipAmount, int pageSize)
        {
            return await _context.Users
                .Include(u => u.Productos)
                .OrderBy(u => u.Id)
                .Skip(skipAmount)
                .Take(pageSize)
                .ToListAsync();
        }

        public override async Task<UserEntity> Insert(UserEntity entity)
        {
            entity.Password = EncryptPassword(entity.Password);
            EntitySet.Add(entity);
            await Save();
            return entity;
        }

        public override async Task Update(UserEntity entity)
        {
            entity.Password = EncryptPassword(entity.Password);
            EntitySet.Entry(entity).State = EntityState.Modified;
            await Save();
        }

        public async Task<UserEntity> GetUserByEmail(string email)
        {

            return await EntitySet.FirstOrDefaultAsync(p => p.Email == email);
        }

        public string EncryptPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public bool VerifyPassword(string password, string hashedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
        }
    }
}
