using Core.Managers;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using WebApp.Controllers;

namespace WebApiUnitTesting
{
    public class UserTesting
    {
        private UserController _userController;
        private readonly IUserManager _userManager;

        [SetUp]
        public void Setup()
        {
            // Inicializar los objetos necesarios
          
            _userController = new UserController(_userManager);
        }

        [Test]
        public void GetAllUser_ReturnsNotNull()
        {
            var result = _userController.GetAllUser();
            Assert.NotNull(result);
        }

        //[Test]
        //public async Task Get_Quantity()
        //{
        //    // Obtener el resultado de la acci�n del controlador
        //    var actionResult = await _userController.GetAllUser();

        //    // Verificar si el resultado es un OkObjectResult y si contiene datos
        //    if (actionResult is OkObjectResult okObjectResult)
        //    {
        //        var data = okObjectResult.Value;

        //        // Verificar si los datos son una lista y si tiene m�s de dos elementos
        //        if (data is IEnumerable<object> enumerable && enumerable.Count() > 2)
        //        {
        //            // El contenido tiene m�s de dos elementos
        //            // Realiza alguna acci�n aqu�...
        //        }
        //        else
        //        {
        //            // El contenido no tiene m�s de dos elementos
        //            // Realiza alguna acci�n aqu�...
        //        }
        //    }
        //    else
        //    {
        //        // El resultado no es un OkObjectResult
        //        // Puedes manejar otros tipos de resultados aqu�...
        //    }
        //}
    }
}
