﻿using Microsoft.AspNetCore.Mvc;
using Core.Managers;
using Core.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {

        public readonly IProductoManager _productsManager;
        private readonly ILogger<ProductoController> _logger;

        public ProductoController(IProductoManager productsManager, ILogger<ProductoController> logger)
        {
            _productsManager = productsManager;
            _logger = logger;
        }
       
        // GET api/<ProductoController>/5
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _productsManager.GetProductByIdAsync(id);
            if(result.IsSuccess)
            {
                _logger.LogInformation($"\nMetodo get (producto) con id={id} ejecutado exitosamente\n");
                return Ok(result.Data);
            }
            _logger.LogWarning($"\nProducto con id: {id} no encontrado\n");
            return NotFound(result.Message);
        }

        // POST api/<ProductoController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductoDTOForPost productoDTO)
        {
            var result = await _productsManager.CreateProductAsync(productoDTO);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return NotFound(result.Message);
        }

        // PUT api/<ProductoController>/5
        //[HttpPut("{id}")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ProductoDTO producto)
        {
            var result = await _productsManager.UpdateProductAsync(producto);

            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return NotFound(result.Message);
        }

        // DELETE api/<ProductoController>/5
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _productsManager.DeleteProductAsync(id);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return NotFound(result.Message);
        }

        //GET List
        [HttpGet]
        public async Task<IActionResult> GetAllProducts()
        {
            var resutl = await _productsManager.GetAllProductsAsync();
            if (resutl.IsSuccess)
            {
                return Ok(resutl.Data);
            }
            return NotFound(resutl.Message);

        }
    }
}
