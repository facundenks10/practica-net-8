﻿using Core.DTOs;
using Core.Managers;
using Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TwoFactorAuthNet;
using TwoFactorAuthNet.Providers.Qr;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public readonly IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }        

        // GET api/<ValuesController>/5
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _userManager.GetUserByIdAsync(id);
            if (result.IsSuccess)
            {
                return Ok(result.Data);
            }
            return NotFound(result.Message);
        }

        // POST api/<ValuesController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDTOForPost user)
        {
            var result = await _userManager.CreateUserAsync(user);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return NotFound(result.Message);
        }

        // PUT api/<ValuesController>/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] UserForUpdateDTO user)
        {
            var result = await _userManager.UpdateUserAsync(user);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return NotFound(result.Message);
        }

        //GET api/<ValuesController>
        [HttpGet]
        public async Task<IActionResult> GetQRUserCode(string email)
        {
            var tfa = new TwoFactorAuth("PracticaAuth", 6, 30, Algorithm.SHA256, new ImageChartsQrCodeProvider());
            var code = tfa.CreateSecret(160);
            var result = await _userManager.UpdateUserCodeAsync(email, code);
            if (result.IsSuccess)
            {
                string imgQR = tfa.GetQrCodeImageAsDataUri(email, code);
                if (!string.IsNullOrEmpty(imgQR) && imgQR.StartsWith("data:image/png;base64"))
                {
                    imgQR = imgQR.Replace("data:image/png;base64,", ""); // Remove the prefix before conversion
                    byte[] picture = Convert.FromBase64String(imgQR);
                    return File(picture, "image/png");
                }
                else
                {
                    return NotFound("QR code image not found or invalid format.");
                }
            }
            else
            {
                return NotFound("Failed to update user code.");
            }
        }

        //GET api/<ValuesController>
        [HttpGet]
        public async Task<IActionResult> GetQRUserCodeHTML(string email)
        {
            var tfa = new TwoFactorAuth("PracticaAuth", 6, 30, Algorithm.SHA256, new ImageChartsQrCodeProvider());
            var code = tfa.CreateSecret(160);
            var result = await _userManager.UpdateUserCodeAsync(email, code);
            if (result.IsSuccess)
            {
                string imgQR = tfa.GetQrCodeImageAsDataUri(email, code);
                if (!string.IsNullOrEmpty(imgQR) && imgQR.StartsWith("data:image/png;base64"))
                {
                    return Ok(new { qrCodeUrl = imgQR });
                }
                else
                {
                    return NotFound("QR code image not found or invalid format.");
                }
            }
            else
            {
                return NotFound("Failed to update user code.");
            }
        }

        //GET api/<ValuesController>
        [HttpGet]
        public async Task<bool> ValidateCode(string email, string codeToValidate)
        {
            var user = await _userManager.GetUserByEmailAsync(email);
            if (user != null)
            {
                string code = user.Data.Code;
                var tfa = new TwoFactorAuth("PracticaAuth", 6, 30, Algorithm.SHA256);
                return tfa.VerifyCode(code, codeToValidate);
            }
          
            return false;
        }

        //POST api/<ValuesController>
        [HttpPost]
        public async Task<bool> Login([FromBody] LoginRequest request)
        {
            var user = await _userManager.GetUserByEmailAsync(request.Email);
            if (user != null)
            {
                bool passwordMatch = BCrypt.Net.BCrypt.Verify(request.Password, user.Data.Password);
                return passwordMatch;
            }
            return false;
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _userManager.DeleteUserAsync(id);
            if (result.IsSuccess)
            {
                return Ok(result.Message);
            }
            return NotFound(result.Message);
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public async Task<IActionResult> GetAllUser()
        {
            var resutl = await _userManager.GetAllUsersAsync();
            if (resutl.IsSuccess)
            {
                return Ok(resutl.Data);
            }
            return NotFound(resutl.Message);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserWithProductos(int page)
        {
            var resutl = await _userManager.GetUserWithProductos(page);
            if (resutl.IsSuccess)
            {
                return Ok(resutl.Data);
            }
            return NotFound(resutl.Message);
        }
    }
}
