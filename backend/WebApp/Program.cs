using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Reflection;
using Data.Db;
using Core.Managers;
using System.Text.Json.Serialization;
using Core;
using NLog.Web;
using NLog;
using Data.Repositories;

// AGREGO NLOGGER
var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("Init API");

try
{
    var builder = WebApplication.CreateBuilder(args);
    //agrego variables de entorno
    builder.Configuration.AddEnvironmentVariables();
    //agrego cadena de conexion
    var connectionString = Environment.GetEnvironmentVariable("DEFAULT_CONNECTION") ?? builder.Configuration.GetConnectionString("conexionDB");
    //agrego contexto y configuracion 
    builder.Services.AddDbContext<DexDbContext>(opt => opt
        //.UseLazyLoadingProxies()
        .ConfigureWarnings(warnings => warnings.Ignore(CoreEventId.DetachedLazyLoadingWarning))
        //.UseSqlServer(builder.Configuration.GetConnectionString("conexionDB")));
        .UseSqlServer(connectionString, builder => builder.MigrationsAssembly("Data")));

    // Add services to the container.
    //AGREGO MIS SERVICIOS PARA INYECCION DE DEPENDENCIAS
    builder.Services.AddTransient<IUserManager, UserManager>();
    builder.Services.AddTransient<IProductoManager, ProductoManager>();

    //AGREGO SERVICIO PARA EL REPOSITORY
    builder.Services.AddScoped(typeof(IRepositoryAsync<>), typeof(RepositoryAsync<>));//genérico en tiempo de compilación
    builder.Services.AddScoped<IProductRepository, ProductRepository>();
    builder.Services.AddScoped<IUserRepository, UserRepository>();

    //SE OBTIENEN TODOS LOS PERFILES PARA AUTOMAPPER
    builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());//AutoMapper para todos los perfiles
    //AGREGO PERFIL MANUALMENTE 
    builder.Services.AddAutoMapper(x => x.AddProfile(new AutoMapping()));
    // Add services to the container.
    builder.Services.AddControllers();

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    builder.Logging.ClearProviders();//eliminan todos los proveedores de registro predeterminados
    builder.Host.UseNLog();//configura NLog como el proveedor de registro

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }


    app.UseCors(builder => builder
           .AllowAnyHeader()
           .AllowAnyMethod()
           .AllowAnyOrigin()
        );

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception e)
{
    logger.Error("El programa se detuvo porque: ", e.Message);
    throw;
}
finally
{
    NLog.LogManager.Shutdown();
}

