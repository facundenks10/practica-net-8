import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';
  code: string = '';
  view: boolean = false;
  backgroundCharge: string = '';
  view_Qr: boolean = false;
  img_qr: string = '';
  constructor(private router: Router,  private user: DataService) {}

  ngOnInit(): void {
  }

  login() {
    let body = {
      'email': this.username,
      'password': this.password
    }
    this.user.login(body).subscribe((res) => {
      if (!res) {
        this.view_Qr = true;
        this.user.generateCode(this.username).subscribe((res) => {
          //this.view_Qr = true;
          console.log(res);
          
          this.img_qr = res.qrCodeUrl;
          
        });
      }
    })
  }

  sendCode(){
    console.log('send code');
    
  }

  onSubmit(form: NgForm) {
    this.view = true;
    this.backgroundCharge = 'backgroundCharge';
    setTimeout(() => {
      this.view = false;
      this.backgroundCharge = '';
      this.router.navigate(['/main/dashboard']);
    }, 1000);
  }
}
