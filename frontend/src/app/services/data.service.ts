import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  constructor(private http: HttpClient) { }

  getData(): Observable<any> {
    return this.http.get('assets/all.json');
  }

  getUsers(): Observable<any> {
    return this.http.get<any>('http://localhost:16340/api/User/GetAllUser');
  }

  login(body: any): Observable<any>{
    return this.http.post<any>('http://localhost:16340/api/User/Login', body);
  }

  generateCode(param: any): Observable<any>{
    return this.http.get('http://localhost:16340/api/User/GetQRUserCodeHTML?email='+ param);
  }
}
